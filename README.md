# Final Project
### Jacob Crump

This project extracts information from two text files: a .data file and a .names file

The .data file contains lines of code where each line a common delimited list. These are data points for different data. 
"mid,low,excellent,mid,stable,stable,stable,15,A
mid,high,excellent,high,stable,stable,stable,10,S"

The .names file contains the meta-data about the data in the .data file. There is a specific part of that file that contains the name, 
description (in paranthases next to the name), and possible other information underneath. Here's a sample of that section:
"7. Attribute Information:
1. L-CORE (patient's internal temperature in C):
high (> 37), mid (>= 36 and <= 37), low (< 36)
2. L-SURF (patient's surface temperature in C):
high (> 36.5), mid (>= 36.5 and <= 35), low (< 35)"

Each name is distiguished by the fact that there is a number in front of it.
The meta data has to at least have a name/description and some descriptor in parantheses. Without both groups in the regex, the input part 
of the program will crash.
The input function will need to know a part of the line before the meta data and a part of the line after. 

This program can do the following things:
Come up with the percentages of the different data points and print them, with the appropriate meta data, to a text file; Generate a pdf with a bar chart for each category; Generate a histogram for a given category; Capture meta data and information into a local database.

The bar chart and statistical functions only work when the data is categorial. The histogram works best for integer and float values.

The interface presents two file options as samples. More samples can be added to the code. Depending on the choice, you can either export stats and bar charts or generate a histogram. Then the information is captured into a local database. 

The sample files were found on the UCI Machine Learning Repository. https://archive.ics.uci.edu/ml/datasets.html