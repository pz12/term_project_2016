class Meta_data(object):
    '''This object holds the name, definition, and possible interpretation for results, all for a particular meta-datum'''
    def __init__(self, abrev='', defin='', interp=''):
        self.abrev = abrev
        self.defin = defin
        self.interp = interp
    
    def get_abrev(self):
        '''Returns the abbreviation of meta-data'''
        return self.abrev
    def get_defin(self):
        '''Returns the definition of meta-data'''
        return self.defin
    def get_interp(self):
        '''Returns the interpration of associated data'''
        return self.interp
    
    def __str__(self):
        '''Returns a string of the information of the meta data'''
        txt = "Name: "+self.abrev
        if self.defin !='':
            txt += "\nDefinition/Units: "+self.defin
        if self.interp != '':
            txt += "\nInterpretation: "+self.interp
        return txt