import matplotlib.pyplot as plt
import numpy as np


def percentages(lis):
    '''
    Returns the percentages of all the unique items in the list
    lis: list of information 
    '''
    percents = []
    nums = []
    coun = []
    names = []
    for x in lis:
        nums.append(x)
        if nums.count(x) <= 1:  #So the count goes in list only once
            coun.append(lis.count(x))
            names.append(x)
    for i in range(len(coun)):
        percents.append(names[i]+": "+str((round((int(coun[i])/sum(coun)*100),2)))+' percent')
    return percents

def bar_chart(lis):
    '''
    Returns a bar chart of a given list
    lis: list of information
    '''
    nums = []
    coun = []
    elements = []
    fig = plt.figure()
    for x in lis:
        nums.append(x)
        if nums.count(x) <= 1:  #So the count goes in list only once
            coun.append(lis.count(x))
            elements.append(x)
    plt.bar(range(len(set(lis))), coun, .75, align = "center")
    length = np.arange(len(set(lis)))
    plt.ylabel("Frequency")
    plt.xlabel("Indicator")
    plt.xticks(length, elements)
    return fig

def histo(lis, name):
    '''
    Returns a histogram of a given list and sets a title
    lis: list of information
    name: desired title for the histogram
    '''
    fig = plt.figure()
    plt.hist(np.asarray([int(x) for x in lis]))
    plt.title(name)
    plt.xlabel("Value")
    plt.ylabel("Frequency")
    return fig