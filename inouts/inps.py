from meta_cl import Meta_data

def inputs(file_names, file_data, start, end, pattern):
    #Read in the files line by line
    with open(file_names) as f0:
        names = f0.readlines()
    with open(file_data) as f1:
        data = f1.readlines()
    #Find the index values of the beginnings of the start and stop spots which contain the meta data
    index_start = [i for i,s in enumerate(names) if start in s]
    index_end = [i for i,s in enumerate(names) if end in s]
    #Capture a list of each line within the start and stop
    meta_data = names[index_start[0]+1:index_end[0]-1]
    meta_list = []
    check = False  #Used to keep track of whether the line is on the "name" or other lines
    for x in range(len(meta_data)):
        st1 = meta_data[x].strip()  #want to get rid of any \n or \t before the line
        if st1[0].isdigit():   #looks to see if this is a "name" and "definition/units" by looking to see if there is a number in front
            lis = []
            abrev = pattern.findall(st1)[0][0]  #Captures the "name"
            defin = pattern.findall(st1)[0][1]  #Captures the "name" and "definition/units" 
            check = False
        else:
            check = True  #Will now capture other modifying meta-data if the line is not part of the "name
        if check == True:
            lis.append(meta_data[x].strip())  #appends all possible modifying meta_data
        if x < len(meta_data)-1:  #Don't want to check past the length of the list of data
            nex = meta_data[x+1].strip()
            if nex[0].isdigit():  #Want to see if the next line is a "name" and "definition/units"
                interp = ' '.join(lis)  #Joins the list with spaces into a string
                meta_list.append(Meta_data(abrev, defin, interp)) #appends a new object 'Meta_data'      
        if x == len(meta_data)-1:  #Checks if this is the last element, if so, it won't look for the next element and just append the                                             object
            interp = ' '.join(lis)
            meta_list.append(Meta_data(abrev, defin, interp))
    all_info = []
    for x in range(len(meta_list)):
        all_info.append([])  #Creates a list of empty lists for each category
    #now to make the list of lists of the different data
    for line in data:
        line1 = line.strip()
        split = line1.split(',')   #makes lists that are created by spliting the string by the colons

        for y in range(len(meta_list)):
            all_info[y].append(split[y])   #Each diagnosis is put into its appropriate list
    return (meta_list, all_info)




