from matplotlib.backends.backend_pdf import PdfPages
from analysiss import *

def out_bar_charts(info, meta_list, file_name):
    '''Outputs a pdf of the bar charts of each data type
    
    info: A list of lists of all of the information from the text file
    meta_list: list of meta data objects
    file_name: the name of the file of the pdf to be exported
    '''
    plot_list = []
    for x in info:
        plot_list.append(bar_chart(x))
    # Put all of the bar charts into a pdf using PdfPages
    with PdfPages(file_name) as pdf:
        for y in range(len(plot_list)):
            plot_list[y].suptitle(meta_list[y].get_abrev(), fontsize=20)  #Change the chart title to the name of the meta data
            pdf.savefig(plot_list[y])
    return

def out_stats(info, meta_list, file_name):
    '''Outputs a text file of the percentages of all data and the meta data
    
    info: A list of lists of all of the information from the text file
    meta_list: list of meta data objects
    file_name: the name of the file of the pdf to be exported
    '''
    with open(file_name, "w") as fout:
        fout.write("Statistics of the Post-Operative Data\n\n")
        for x in range(len(meta_list)):
            fout.write(meta_list[x].get_abrev()+'\n')
            fout.write(meta_list[x].get_defin()+'\n')
            if meta_list[x].get_interp() == '':    #won't put an enter in if the line is empty
                fout.write(meta_list[x].get_interp())
            else:
                fout.write(meta_list[x].get_interp()+'\n') 
            for stat in percentages(info[x]):
                fout.write(stat+'\n')
            fout.write('\n')
    return 
    