'''This is the __init__ file for the input/output'''

__all__ = ["inps", "outs"]
from .inps import *
from .outs import *