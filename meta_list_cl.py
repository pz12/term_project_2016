from meta_cl import Meta_data

class Meta_list_data(Meta_data):
    def __init__(self, abrev='', defin='', interp='', lst=[]):
        super(Meta_list_data, self).__init__(abrev=abrev, defin=defin, interp=interp)
        self.lst = lst
        
    def get_list(self):
        '''Returns the data list'''
        return self.lst
    
    def __str__(self):
        '''Returns a string of the information of the meta data'''
        txt = "Name: "+self.abrev
        if self.defin !='':
            txt += "\nDefinition/Units: "+self.defin
        if self.interp != '':
            txt += "\nInterpretation: "+self.interp
        txt += "\n"+str(self.lst)
        return txt